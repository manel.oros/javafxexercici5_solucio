/* 
 * Autor: Manel Orós
 * Basat en la implementació original de Mike Lowe
 * Basat en l'autòmat cel.lular de John Horton Conway
 * Sota llicencia GPL 3.0
 */
package dam2.javafx_jocdelavida;

import java.util.ArrayList;
import java.util.List;

/**
 * És el model de dades.
 * "Habitat" on es desenvolupen els essers.
 * Implementa una matriu on es desenvolupen els essers i les regles que els regulen la vida
 * Principalment és una matriu bidimensional de nfiles i mcolumnes que s'utilitza com a base del joc
 */
public class Model {
    
    //<editor-fold defaultstate="collapsed" desc="---------- atributs privats ------------">
    private Boolean[][] habitat;
    private int maxFiles;
    private int maxCols;
    //</editor-fold>
    
    
    /**
     * Constructor
     * @param files files de la matriu 
     * @param cols columnes de la matriu
     */
    public Model (int files, int cols)
    {
        this.maxFiles = files;
        this.maxCols = cols;
        this.habitat = new Boolean[maxFiles][maxCols];
    }
    
    //<editor-fold defaultstate="collapsed" desc="---------- metodes publics ------------">
    /**
     * Inicialitza l'habitat (matriu) o be amb una matriu neta
     * o be amb una matriu habitada random (15% de població)
     * @param aleatori si true, matriu habitada random. Si false, matriu neta
     */
    public void initCelules(Boolean aleatori)
    { 
        for (int fila = 0; fila < getMaxFiles(); fila++) 
        {
            for (int col = 0; col < getMaxCols(); col++) {
                getHabitat()[fila][col] = (aleatori && Math.random() > 0.85);
            }
        }
    }
    
    /**
     * Assigna a totes les celules els estats futurs
     * 
     * @param normes
     */
    public void assignaProperEstat(int normes)
    {
      this.setHabitat(properEstat(normes));
    }
    
        //<editor-fold defaultstate="collapsed" desc="---------- getters i setters ------------">
     /**
     * @return the maxFiles
     */
    public int getMaxFiles() {
        return maxFiles;
    }

    /**
     * @param maxFiles the maxFiles to set
     */
    public void setMaxFiles(int maxFiles) {
        this.maxFiles = maxFiles;
    }

    /**
     * @return the maxCols
     */
    public int getMaxCols() {
        return maxCols;
    }

    /**
     * @param maxCols the maxCols to set
     */
    public void setMaxCols(int maxCols) {
        this.maxCols = maxCols;
    }
    
    /**
     * Retorna una celula (posició) en funció de la fila i la columna proporcionades.
     * Aquest mètode te en compte l'estat "plegat" o infinit de l'habitat. Exemple:
     * Si el rang de la matriu és 0..49 files i 0..49 columnes i se li demana la posició (25,50),
     * retornarà la posició (25,0), entenent que ens hem sortit per la dreta i apareixem per l'esquerra.
     * @param fil
     * @param col
     * @return objecte celula en la posició demanada
     */
    public Boolean getCelula(int fil, int col)
    {
        int filaPlegada = (fil+this.getMaxFiles())%this.getMaxFiles();
        int columnaPlegada = (col+this.getMaxCols())%this.getMaxCols();
        return (this.getHabitat()[ filaPlegada ][ columnaPlegada] );
    }
    
    /**
     * Inverteix el valor d'una celula en funció de la fila o la columna proporcionades.
     * Aquest mètode te en compte l'estat "plegat" o infinit de l'habitat. Exemple:
     * Si el rang de la matriu és 0..49 files i 0..49 columnes i se li demana la posició (25,50),
     * retornarà la posició (25,0), entenent que ens hem sortit per la dreta i apareixem per l'esquerra.
     * @param fil
     * @param col
     */
    public void flipCelula(int fil, int col)
    {
        int filaPlegada = (fil+this.getMaxFiles())%this.getMaxFiles();
        int columnaPlegada = (col+this.getMaxCols())%this.getMaxCols();
        
        //inverteix el valor
        this.getHabitat()[ filaPlegada ][ columnaPlegada] = !(this.getHabitat()[ filaPlegada ][ columnaPlegada]);
        
    }

    /**
     * @return the habitat
     */
    public Boolean[][] getHabitat() {
        return habitat;
    }

    
    public void setHabitat(Boolean[][] habitat) {
        this.habitat = habitat;
    }
    //</editor-fold>
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="---------- metodes privats ------------">
    
    /**
     * En funció dels estats actuals estableix els propers estats de les celules, 
     * aplicant les regles preestablertes.
     * 
     * @return propers estats de les celules
     */
    private Boolean[][] properEstat(int normes) 
    {
        Boolean[][] resultat = new Boolean[maxFiles][maxCols];
        Boolean isAliveInNextState = false;
        
        //recorrem totes les celules de la matriu
        for (int fil = 0; fil < maxFiles; fil++) 
        {
          for (int col = 0; col < maxCols; col++) 
          {
            //recollim la informació de la celula
            Boolean isViva = getCelula(fil, col);
            
            //establim quants veins vius te la celula
            int veinsVius = comptaVeinsVius(fil, col);
            
            // set de normes 1 o 2
            switch (normes) {
                //Si estaba viva i té dos veïns vius, segueix viva. Si estaba morta i té tres veins vius, resucita. En qualsevol altre cas, mor.
                case 1 -> isAliveInNextState = ((isViva == true && veinsVius == 2) || veinsVius == 3);
                //Si estaba viva i té tres veïns vius, segueix viva. Si estaba morta i té quatre veins vius, resucita. En qualsevol altre cas, mor.
                case 2 -> isAliveInNextState = ((isViva == true && veinsVius == 3) || veinsVius == 4);
                //todo default: throw exception i tal
            }
            
            resultat[fil][col] = isAliveInNextState;
          }
        }

        return resultat;
    }
    
    /**
     * Donada una cela de la matriu, compta els veins vius que te al voltant en el moment actual
     * @param fil
     * @param col
     * @return numero de beins vius
     */
    private int comptaVeinsVius(int fil, int col) 
    {
        //obtenim els veins
        List<Boolean> veins = getVeins(fil, col);
        
        //comptem els vius
        return (int)veins.stream().filter(x -> x == true).count(); //es pot abreviar amb filter(x -> x) però no s'entén gaire...
    }
    
    /**
     * Donada una determinada posició a la matriu 
     * determina les posicions dels 8 veins d'aquesta
     * 
     * 
     *   OOO
     *   OXO
     *   OOO
     * 
     * @param fil
     * @param col
     * @return array amb les posicions dels 8 veins començant per la immediatament superior
     * i en sentit de les agulles del rellotge
     */
    private List<Boolean> getVeins(int fil, int col) 
    {
        int north = fil - 1;
        int east = col + 1;
        int south = fil + 1;
        int west = col - 1;
        
        List<Boolean> ret = new  ArrayList<>(8);
        
        ret.add(getCelula(north, east));
        ret.add(getCelula(north, col));
        ret.add(getCelula(north, west));
        ret.add(getCelula(fil, east));
        ret.add(getCelula(fil, west));
        ret.add(getCelula(south, east));
        ret.add(getCelula(south, col));
        ret.add(getCelula(south, west));
        
        return ret;
    }  
    
 //</editor-fold>
    
}
