/**
 * 
 * Autor: Manel Orós
 * Basat en la implementació original de Mike Lowe
 * Basat en l'autòmat cel.lular de John Horton Conway
 * Sota llicencia GPL 3.0
 * Darrera rev 10/2023
 **/
package dam2.javafx_jocdelavida;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


/***
 * 
 * @author Manel Orós Cuenca <manel.oros@copernic.cat>
 * 
 * Classe principal que controla l'inici i finalització de l'aplicació
 */
public class MainJoc extends Application {
    
    private static final String RUTA_VISTA = "vistaMon.fxml";
    private static final String NOM_APP = "JOC DE LA VIDA. 2DAMT.";
    
    @Override
    public void start(Stage stage) throws IOException {
        
        // Carreguem la vista inicial
        FXMLLoader fxmlLoader = new FXMLLoader(MainJoc.class.getResource("vistaMon.fxml"));
        
        Parent root  = fxmlLoader.load();
        
        Controlador c = fxmlLoader.getController();
        
        // Creem l'objecte escena i li passem la vista
        Scene scene = new Scene(root);
        
        //no espot canviar de grandaria
        stage.setResizable(false);
        
        //assignem la vista a l'escenari
        stage.setScene(scene);
        
        // mostrem l'escenari
        stage.show();
        
    }

    public static void main(String[] args) {
       
        Application.launch();
       
    }

    @Override
    public void init() throws Exception {
        
    }
    
    @Override
    public void stop() throws Exception {
        
    }
}
