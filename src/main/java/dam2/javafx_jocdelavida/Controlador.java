/* 
 * Autor: Manel Orós
 * Basat en la implementació original de Mike Lowe
 * Basat en l'autòmat cel.lular de John Horton Conway
 * Sota llicencia GPL 3.0
 */
package dam2.javafx_jocdelavida;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.util.Duration;



/***
 * Classe controlador de la vista FXML
 * També gestoina la lógica principal dels elements del joc
 * @author Manel Orós Cuenca <manel.oros@copernic.cat>
 */
public class Controlador implements Initializable{
    
    //<editor-fold defaultstate="collapsed" desc="----------- Atributs privats -------------">
    private final int COLUMNES = 100;
    private final int FILES = 100;
    private Model habitat;
    private Timeline controlTempo;
    private KeyFrame keyFrame;
    private int comptadorGeneracio;
    private int normes;
    private Boolean isPlaying;
    //</editor-fold>

   
    //<editor-fold defaultstate="collapsed" desc="------------ Elements FXML ---------------">
    @FXML
    private Slider sVelocitatMilisegons;
    
    @FXML
    private Button btnInici;

    @FXML
    private Button btnPausa;
    
    @FXML
    private Button btnReset;
    
    @FXML
    private Button btnRandom;
    
    @FXML
    private Label lblGeneracio;
    
    @FXML 
    private GridPane matriuGrafica;
    
    @FXML
    private RadioButton rbNormes1;
    
    @FXML
    private RadioButton rbNormes2;
    
    @FXML
    private ToggleGroup tgNormes;
    
    @FXML
    private Label lblDelay;
    
    @FXML
    public void pressedPlayButtonAction()
    {
        botoPlayPremut();
        controlTempo.play();
    }
    
    @FXML
    public void pressedPausaButtonAction()
    {
        botoPausePremut();
         controlTempo.pause();
    }
    
    @FXML
    public void pressedResetButtonAction()
    {
        this.getHabitat().initCelules(false);
        regeneraHabitat();
        comptadorGeneracio = 0;
        refreshLabelGeneracio();
    }
    
    @FXML
    public void pressedRandomButtonAction()
    {
        this.getHabitat().initCelules(true);
        regeneraHabitat();
        comptadorGeneracio = 0;
        refreshLabelGeneracio();
    }
    
    //</editor-fold>
  
    
    //<editor-fold defaultstate="collapsed" desc="--------- Mètodes públics -----------">

    /**
     * Mètode que es crida abans de finalitzar el controlador per 
     * tal de parar els processos en marxa, etc...
     */
    public void stopProcessos()
    {
        controlTempo.stop();
    }
    
    /**
     * Estat de la botonera quan es prem Play
     */
    public void botoPlayPremut()
    {
        this.normes = obtainNormes();
        btnInici.setDisable(true);
        btnPausa.setDisable(false);
        btnReset.setDisable(true);
        btnRandom.setDisable(true);
        sVelocitatMilisegons.setDisable(true);
        rbNormes1.setDisable(true);
        rbNormes2.setDisable(true);
        isPlaying = true;
       
        
    } //pressedDaemonButtonAction
    
    /**
     * Estat de la botonera quan es prem pause
     */
    public void botoPausePremut()
    {
        btnInici.setDisable(false);
        btnPausa.setDisable(true);
        btnReset.setDisable(false);
        btnRandom.setDisable(false);
        sVelocitatMilisegons.setDisable(false);
        rbNormes1.setDisable(false);
        rbNormes2.setDisable(false);
        isPlaying = false;
    }
    
    /**
    * Avança l'habitat una generació
    */
    public void seguentGeneracio()
    {
        this.getHabitat().assignaProperEstat(this.normes); 
        updateHabitat(this.getHabitat());
        comptadorGeneracio++;
        refreshLabelGeneracio();
    }
    
    //<editor-fold defaultstate="collapsed" desc="----------- getters i setters ------------------ ">
    

    /**
     * 
     * @return array bidimensional habitat
     */
    public Model getHabitat() {
        return habitat;
    }

    /**
     * Estableix habitat
     * @param habitat 
     */
    public void setHabitat(Model habitat) {
        this.habitat = habitat;
    }
    
    //</editor-fold>
    //</editor-fold>
   
    
    //<editor-fold defaultstate="collapsed" desc="---------- Mètodes privats ------------">

    /**
     * Inicialitza els valors per defecte del control visual GridPane
     * tenint en compte les dimensions de l'habitat
     * @param h habitat
     */
    private void initGridPane()
    {
        int numCols = this.habitat.getMaxCols() ;
        int numFiles = this.habitat.getMaxFiles();
        
        for (int i = 0; i < numCols; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            colConst.setPercentWidth(100.0 / numCols);
            matriuGrafica.getColumnConstraints().add(colConst);
        }
        for (int i = 0; i < numFiles; i++) {
            RowConstraints rowConst = new RowConstraints();
            rowConst.setPercentHeight(100.0 / numFiles);
            matriuGrafica.getRowConstraints().add(rowConst);         
        }
        
        /***
         * Esdeveniment necessàri per a detectar el mode "pintura".
         * Quan tenim el botó primeri del mouse, "pintem" cel.les de negre.
         * 1- Ens suscribim a l'esdeveniment OnDragDetected 
         * 2- Un cop s'activa l'esdeveniment, verifiquem que el botó primari està premut
         * 3- Consumim l'esdeveniment per tal que no es propagui més enllà (als nodes Pane)
         * 4- Iniciem la ràfaga d'esdeveniments de tipus "drag" als nodes fills (els Pane)
         * 5- Aquests esdeveniments han de ser consumits pels nodes "fills" (veure mètode regeneraHabitat)
         * 
         */
        matriuGrafica.setOnDragDetected(evt -> {
          if (evt.getButton() == MouseButton.PRIMARY) {
            /***
             * Events are passed along a specific route. 
             * In most cases (e.g. mouse/key events) The route will start at the root Node of the Scene and contain every Node on the path from the root Node to the target Node in the scene graph. 
             * On the route to the target Node, event filters are executed and if any of those filters should consume the event, this stops any further handling of the event. 
             * Once the event has reached the target Node if "travels" back to the root calling any event handler along the way. The event handling can be stopped there too by consuming the event.
             * 
             * source: https://stackoverflow.com/questions/37813358/what-is-the-meaning-of-event-consumes-in-javafx
             */
            evt.consume();
            
            
            /***
             * Full press-drag-release gesture can be started by calling startFullDrag() (on a node or scene) inside of a DRAG_DETECTED event handler. 
             * This call activates delivering of MouseDragEvents to the nodes that are under cursor during the dragging gesture.
             * source: https://openjfx.io/javadoc/13/javafx.graphics/javafx/scene/input/MouseDragEvent.html
             */
            matriuGrafica.startFullDrag();
          }
        } );
        
        regeneraHabitat();
    }
    
    /**
     * Elimina les cel-les anteriors i regenera de nou el GridPane en base a l'habitat
     * @param h habitat
     * 
     */
    private void regeneraHabitat()
    {
        
        int maxCol = this.habitat.getMaxCols() ;
        int maxFil = this.habitat.getMaxFiles();
               
        matriuGrafica.getChildren().clear();
        
        for (int fil = 0; fil < maxFil; fil++) {
              for (int col = 0; col < maxCol; col++) {
                  Pane cellPane = new Pane();
                  
                  /***
                   * Esdeveniment de fer click (i no drag) sobre una cel.la
                   */
                  cellPane.setOnMouseClicked(e -> {
                      //si no esta en marxa
                      if (!this.isPlaying)
                      {
                        Pane p = (Pane)e.getSource();
                        
                        //invertim el valor de la celula
                        this.habitat.flipCelula(GridPane.getRowIndex(p), GridPane.getColumnIndex(p));
                        
                        //actualitzaem la seva visualització
                        updateCelula(p, this.habitat);
                      }
                  });

                  /***
                   * El consum d'aquest esdeveniment va directament vinculat al mètode
                   * startFullDrag() iniciat pel node pare (GridPane).
                   */
                  cellPane.setOnMouseDragEntered(e -> {
                      //si no esta en marxa
                      if (!this.isPlaying)
                      {
                        Pane p = (Pane)e.getSource();
                        
                        //resucitem cel.la
                        this.habitat.getHabitat()[GridPane.getRowIndex(p)][GridPane.getColumnIndex(p)] = true;
                        
                       //repintem la casella en funcio de  l'habitat
                       // updateCelula(p, this.habitat);
                       updateCelula(p, this.habitat);
                      }
                  });
                   
                   
                  colorPane((Node)cellPane, this.habitat.getCelula(fil, col));

                  //ULL!! A GridPane.add va primer la columna i despres la fila
                  matriuGrafica.add(cellPane,col,fil); 
              }
        }
    }
    
    /***
    * S'actualitza la vista (Pane) en funció del valor del model (habitat) per a totes 
    * les cel.les
    * 
    * 
    * @param h 
    */
    private void updateHabitat(Model h)
    {
        for (Node n : matriuGrafica.getChildren())
        {
            colorPane(n,h.getCelula(GridPane.getRowIndex(n), GridPane.getColumnIndex(n)));
        }
    }
    
    /***
     * S'actualitza la vista (Pane) en funció del valor del model (habitat)
     * per una sola cel.la
     * @param p cel.la
     * @param h habitat
     */
    private void updateCelula(Pane p, Model h)
    {  
        colorPane(p, h.getCelula(GridPane.getRowIndex(p), GridPane.getColumnIndex(p)));
    }
    
    /***
     * Determina el color de la cela en funció si viva o morta
     * @param n
     * @param isViva 
     */
    private void colorPane(Node n, boolean isViva)
    {
         if (isViva)
            n.setStyle("-fx-background-color:black");
        else
            n.setStyle("-fx-background-color:white");
    }
    
    /**
     * Simula el pas d'una generacio a un altre.
     * Inicialitza un esdeveniment que cada cert temps executa el mètode seguentGeneracio(). 
     * Utilitza la classe KeyFrame, que genera un esdeveniment cada x temps que realitza una acció.
     * A,b eś mètodes play, pause i sto ppermet controlar el KeyFrame.
     * @param milisegons 
     */
    private void initControlTempo(int milisegons) {
        
        Duration duracio = new Duration(milisegons);
        EventHandler<ActionEvent> eventHandler = event -> seguentGeneracio();
        keyFrame = new KeyFrame(duracio, eventHandler);
        controlTempo = new Timeline(keyFrame);
        controlTempo.setCycleCount(Animation.INDEFINITE); 
    }
    
   
    /**
     * Regenera el missatge del numero de generacions en curs
     */
    private void refreshLabelGeneracio()
    {
         lblGeneracio.setText("Generacions: " + Integer.toString(comptadorGeneracio));
    }
    
    /**
     * Retorna l'estat del radiobutton de normes
     * @return 1 si normes estandard, 2 si normes especials
     */
    private int obtainNormes() {
        
        switch (((RadioButton) tgNormes.getSelectedToggle()).getId()) {
            case "rbNormes1" -> {
                return 1;
            }
            case "rbNormes2" -> {
                return 2;
            }
            default -> {
                return 1;
            }
        }
    }

 
    /***
     * Inicialitza el controlador amb els valors per defecte
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //controls
        sVelocitatMilisegons.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            initControlTempo(newValue.intValue());
            String s =  String.format("%s",newValue.intValue());
            lblDelay.setText(s);
           
        });
        sVelocitatMilisegons.setValue(500);
        
        // dimensions inicials de la matriu 
        this.habitat = new Model(this.COLUMNES,this.FILES);
        this.habitat.initCelules(Boolean.TRUE);
        initGridPane();
       
        this.botoPausePremut();
        refreshLabelGeneracio();
        
    }
}
